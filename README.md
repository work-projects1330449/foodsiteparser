# Parser for website with food calories

## Structure
- main.py&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;--- parser file
- index.html&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;--- webpage with food categories
- all_categories_dict.json&emsp;&emsp;--- json with links on each category
- data&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;--- folder with collected data from categories in 3 formats: .csv, .json, .html
